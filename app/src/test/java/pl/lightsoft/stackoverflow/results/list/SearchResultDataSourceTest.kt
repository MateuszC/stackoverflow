package pl.lightsoft.stackoverflow.results.list

import android.arch.paging.DataSource
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.*
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import pl.lightsoft.stackoverflow.results.model.SearchResult
import pl.lightsoft.stackoverflow.results.model.SearchResults

/**
 * Created by Mateusz on 2017-12-05.
 */
@RunWith(MockitoJUnitRunner::class)
class SearchResultDataSourceTest {
    @Mock
    lateinit var questionSearcher: QuestionsSearcher

    lateinit var searchResultDataSource: SearchResultDataSource

    private val query = "testQuery"

    @Before
    fun setUp() {
        searchResultDataSource = SearchResultDataSource(questionSearcher, query)
    }

    @Test
    fun `count items should return count items from search response`() {
        val totalItems = 101
        Mockito.`when`(questionSearcher.searchQuestions(anyString(), anyInt(), anyInt()))
                .thenReturn(SearchResults(totalItems, arrayListOf()))
        val countItems = searchResultDataSource.countItems()
        assertEquals(countItems, totalItems)
    }

    @Test
    fun `count items should return undefined when response is null`() {
        Mockito.`when`(questionSearcher.searchQuestions(anyString(), anyInt(), anyInt()))
                .thenReturn(null)
        val countItems = searchResultDataSource.countItems()
        assertEquals(DataSource.COUNT_UNDEFINED, countItems)
    }

    @Test
    fun `should return empty list of results when first load range failed`() {
        Mockito.`when`(questionSearcher.searchQuestions(anyString(), anyInt(), anyInt()))
                .thenReturn(null)
        val result = searchResultDataSource.loadRange(0, 10)
        assertNotNull(result)
    }

    @Test
    fun `should return null when not first load range failed`() {
        Mockito.`when`(questionSearcher.searchQuestions(anyString(), anyInt(), anyInt()))
                .thenReturn(null)
        val result = searchResultDataSource.loadRange(0, 10)
        assertNull(result)
    }

    @Test
    fun `should return list of result when load range success`() {
        val totalItems = 101
        val count = 2
        Mockito.`when`(questionSearcher.searchQuestions(anyString(), eq(0), eq(count)))
                .thenReturn(SearchResults(totalItems, createSearchResultList(count)))
        val result = searchResultDataSource.loadRange(0, count)
        assertNotNull(result)
        assertEquals(count, result?.size)
    }

    private fun createSearchResultList(size: Int): List<SearchResult> {
        return (1..size).map {
            SearchResult("title" + it, "author" + it, listOf("tag1", "tag2"), it, it)
        }
    }
}