package pl.lightsoft.stackoverflow.results

import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import pl.lightsoft.stackoverflow.results.list.QuestionsSearchPagedListProvider
import pl.lightsoft.stackoverflow.results.list.SearchResultDataSource

/**
 * Created by Mateusz on 02.12.2017.
 */
@RunWith(MockitoJUnitRunner::class)
class QuestionsSearchPagedListProviderTest {
    @Mock
    lateinit var searchResultDataSource: SearchResultDataSource
    private lateinit var questionsSearchPagedListProvider: QuestionsSearchPagedListProvider

    @Before
    fun setUp() {
        questionsSearchPagedListProvider = QuestionsSearchPagedListProvider(searchResultDataSource)
    }

    @Test
    fun `create data source should not return null`() {
        val dataSource = questionsSearchPagedListProvider.createDataSource()
        assertNotNull(dataSource)
    }
}