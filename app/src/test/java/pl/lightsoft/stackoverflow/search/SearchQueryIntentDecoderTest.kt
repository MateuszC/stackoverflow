package pl.lightsoft.stackoverflow.search

import android.app.SearchManager
import android.content.Intent
import android.os.Bundle
import io.reactivex.observers.TestObserver
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

/**
 * Created by Mateusz on 02.12.2017.
 */
class SearchQueryIntentDecoderTest {

    lateinit var searchDecoder: SearchQueryIntentDecoder
    lateinit var testObserver : TestObserver<String>


    @Before
    fun setUp() {
        searchDecoder = SearchQueryIntentDecoder()
        testObserver = TestObserver()
        searchDecoder.searchQuery.subscribe(testObserver)
    }

    @Test
    fun `search query should change when search intent action has query extras`() {
        val searchQuery = "searchQuery"
        val searchIntent = MockIntentBuilder()
                .createActionIntent(Intent.ACTION_SEARCH)
                .putStringExtras(SearchManager.QUERY, searchQuery)
                .create()

        searchDecoder.decodeIntent(searchIntent)
        testObserver.assertNoErrors()
        testObserver.assertValue(searchQuery)
    }

    @Test
    fun `shearch query should not change when intent has not search action`() {
        val searchQuery = "searchQuery"
        val searchIntent = MockIntentBuilder()
                .putStringExtras(SearchManager.QUERY, searchQuery)
                .create()

        searchDecoder.decodeIntent(searchIntent)
        testObserver.assertNoErrors()
        testObserver.assertNever(searchQuery)
    }

    class MockIntentBuilder {

        val intent: Intent = Mockito.mock(Intent::class.java)

        fun createActionIntent(action: String): MockIntentBuilder {
            Mockito.`when`(intent.action).thenReturn(Intent.ACTION_SEARCH)
            return this
        }

        fun putStringExtras(key: String, value: String): MockIntentBuilder {
            val bundle = Mockito.mock(Bundle::class.java)
            Mockito.`when`(bundle.getString(key)).thenReturn(value)
            Mockito.`when`(intent.getStringExtra(key)).thenReturn(value)
            Mockito.`when`(intent.extras).thenReturn(bundle)
            return this
        }

        fun create(): Intent {
            return intent
        }
    }
}