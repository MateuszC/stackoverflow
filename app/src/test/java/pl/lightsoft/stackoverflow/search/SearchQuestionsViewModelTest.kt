package pl.lightsoft.stackoverflow.search

import android.databinding.Observable
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.*
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

/**
 * Created by Mateusz on 2017-11-29.
 */
@RunWith(MockitoJUnitRunner::class)
class SearchQuestionsViewModelTest {
    private lateinit var viewModel: SearchQuestionsViewModel

    @Before
    fun setUp() {
        viewModel = SearchQuestionsViewModel()
    }

    @Test
    fun `should notify search query observers when new query submitted`() {
        var callback = mock(Observable.OnPropertyChangedCallback::class.java)
        viewModel.searchQuery.addOnPropertyChangedCallback(callback)

        var newQuery = "newQuery"
        viewModel.onNewQuerySubmit(newQuery)
        verify(callback).onPropertyChanged(eq(viewModel.searchQuery), anyInt())
        assertEquals(newQuery, viewModel.searchQuery.get())
    }

    @Test
    fun `should not notify search query observers when new query is the same`() {
        var callback = mock(Observable.OnPropertyChangedCallback::class.java)

        var oldQuery = "query"
        viewModel.searchQuery.set(oldQuery)

        var newQuery = "query"
        viewModel.searchQuery.addOnPropertyChangedCallback(callback)
        viewModel.onNewQuerySubmit(newQuery)
        verify(callback, never()).onPropertyChanged(eq(viewModel.searchQuery), anyInt())
        assertEquals(newQuery, viewModel.searchQuery.get())
    }
}