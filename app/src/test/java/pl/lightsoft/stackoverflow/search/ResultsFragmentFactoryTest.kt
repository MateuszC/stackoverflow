package pl.lightsoft.stackoverflow.search

import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import pl.lightsoft.stackoverflow.message.MessageFragment
import pl.lightsoft.stackoverflow.results.SearchResultListFragment

/**
 * Created by Mateusz on 04.12.2017.
 */
class ResultsFragmentFactoryTest {
    private lateinit var resultFragmentFactory: ResultsFragmentFactory

    @Before
    fun setUp() {
        resultFragmentFactory = ResultsFragmentFactory()
    }

    @Test
    fun `should return UserMessageFragment when query is empty`() {
        val someQuery = " "
        val fragment = resultFragmentFactory.createResultFragment(someQuery)
        assertTrue(fragment is MessageFragment)
    }

    @Test
    fun `should return SearchResultListFragment when query is not empty`() {
        val someQuery = "someQuery"
        val fragment = resultFragmentFactory.createResultFragment(someQuery)
        assertTrue(fragment is SearchResultListFragment)
    }

}