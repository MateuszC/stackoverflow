package pl.lightsoft.stackoverflow.application.component.module


import javax.inject.Scope

/**
 * Created by Mateusz on 2017-11-28.
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ApplicationScope
