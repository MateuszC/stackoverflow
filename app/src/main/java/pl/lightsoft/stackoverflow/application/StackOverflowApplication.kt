package pl.lightsoft.stackoverflow.application

import android.app.Activity
import android.app.Application
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import pl.lightsoft.stackoverflow.application.component.DaggerStackOverflowComponent
import pl.lightsoft.stackoverflow.application.configuration.AppConfiguration
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by Mateusz on 2017-11-28.
 */
class StackOverflowApplication : Application(), HasActivityInjector {
    operator fun get(activity: Activity): StackOverflowApplication {
        return activity.application as StackOverflowApplication
    }

    @Inject
    internal lateinit var activityDispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    @Inject
    internal lateinit var configuration: AppConfiguration


    override fun onCreate() {
        super.onCreate()
        configureApplicationComponent()
        configuration.configureApplication(this)
    }

    private fun configureApplicationComponent() {
        DaggerStackOverflowComponent
                .builder()
                .application(this)
                .build()
                .inject(this)
    }

    override fun activityInjector(): AndroidInjector<Activity> {
        return activityDispatchingAndroidInjector
    }

}