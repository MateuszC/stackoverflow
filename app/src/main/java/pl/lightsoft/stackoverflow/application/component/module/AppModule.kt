package pl.lightsoft.stackoverflow.application.component.module

import android.app.Application
import android.content.Context
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import pl.lightsoft.stackoverflow.application.NetworkConfiguration
import pl.lightsoft.stackoverflow.application.configuration.AppConfiguration
import pl.lightsoft.stackoverflow.application.configuration.TimberConfiguration
import pl.lightsoft.stackoverflow.message.component.MessageFragmentComponent
import pl.lightsoft.stackoverflow.results.component.SearchResultListFragmentComponent
import pl.lightsoft.stackoverflow.search.component.SearchQuestionsComponent
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by Mateusz on 2017-11-28.
 */
@Module(includes = arrayOf(
        GsonModule::class,
        NetworkModule::class,
        ContextModule::class),

        subcomponents = arrayOf(
                SearchQuestionsComponent::class,
                SearchResultListFragmentComponent::class,
                MessageFragmentComponent::class))
class AppModule {
    @ApplicationScope
    @Provides
    fun retrofit(okHttpClient: OkHttpClient,
                 gson: Gson,
                 networkConfiguration: NetworkConfiguration): Retrofit {
        return Retrofit.Builder()
                .baseUrl(networkConfiguration.baseUrl)
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
    }

    @ApplicationScope
    @Provides
    fun appConfiguration(): AppConfiguration {
        return TimberConfiguration()
    }

}