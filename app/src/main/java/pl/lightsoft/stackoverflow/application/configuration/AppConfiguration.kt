package pl.lightsoft.stackoverflow.application.configuration

import android.app.Application

/**
 * Created by Mateusz on 2017-11-28.
 */
interface AppConfiguration {
    fun configureApplication(application: Application)
}
