package pl.lightsoft.stackoverflow.application.component

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import pl.lightsoft.stackoverflow.application.StackOverflowApplication
import pl.lightsoft.stackoverflow.application.component.module.AppModule
import pl.lightsoft.stackoverflow.application.component.module.ApplicationScope
import pl.lightsoft.stackoverflow.application.component.builder.BuildersModule

/**
 * Created by Mateusz on 2017-11-28.
 */
@ApplicationScope
@Component(modules = arrayOf(AndroidSupportInjectionModule::class, AppModule::class, BuildersModule::class))
interface StackOverflowComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): StackOverflowComponent
    }

    fun inject(application: StackOverflowApplication)
}
