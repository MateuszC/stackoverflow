package pl.lightsoft.stackoverflow.application.component.module

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides

/**
 * Created by Mateusz on 2017-11-28.
 */
@Module
class GsonModule {
    @Provides
    @ApplicationScope
    fun gson(): Gson {
        return GsonBuilder().create()
    }
}
