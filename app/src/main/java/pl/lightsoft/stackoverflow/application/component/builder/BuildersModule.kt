package pl.lightsoft.stackoverflow.application.component.builder

import dagger.Module
import dagger.android.ContributesAndroidInjector
import pl.lightsoft.stackoverflow.message.MessageFragment
import pl.lightsoft.stackoverflow.message.component.module.MessageFragmentModule
import pl.lightsoft.stackoverflow.message.component.module.MessageFragmentScope
import pl.lightsoft.stackoverflow.results.SearchResultListFragment
import pl.lightsoft.stackoverflow.results.component.module.ResultFragmentScope
import pl.lightsoft.stackoverflow.results.component.module.SearchResultListFragmentModule
import pl.lightsoft.stackoverflow.search.SearchQuestionsActivity
import pl.lightsoft.stackoverflow.search.component.module.SearchActivityScope
import pl.lightsoft.stackoverflow.search.component.module.SearchQueryActivityModule

/**
 * Created by Mateusz on 2017-11-28.
 */
@Module
abstract class BuildersModule {
    @SearchActivityScope
    @ContributesAndroidInjector(modules = arrayOf(SearchQueryActivityModule::class))
    abstract fun bindSearchQuestionsActivity(): SearchQuestionsActivity

    @ResultFragmentScope
    @ContributesAndroidInjector(modules = arrayOf(SearchResultListFragmentModule::class))
    abstract fun bindSearchResultFragment(): SearchResultListFragment

    @MessageFragmentScope
    @ContributesAndroidInjector(modules = arrayOf(MessageFragmentModule::class))
    abstract fun bindMessageFragment(): MessageFragment

}