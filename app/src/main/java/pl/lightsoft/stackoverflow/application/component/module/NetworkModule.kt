package pl.lightsoft.stackoverflow.application.component.module

import android.content.Context
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import pl.lightsoft.stackoverflow.application.NetworkConfiguration
import timber.log.Timber
import java.io.File

/**
 * Created by Mateusz on 2017-11-28.
 */
@Module
class NetworkModule {

    @Provides
    @ApplicationScope
    fun loggingInterceptor(): HttpLoggingInterceptor {
        val interceptor = HttpLoggingInterceptor { message -> Timber.i(message) }
        interceptor.level = HttpLoggingInterceptor.Level.BASIC
        return interceptor
    }

    @Provides
    @ApplicationScope
    fun cacheFile(context: Context): File {
        return File(context.cacheDir, "okhttp_cache")
    }

    @Provides
    @ApplicationScope
    fun cache(cacheFile: File): Cache {
        return Cache(cacheFile, (10 * 1024 * 1024).toLong())
    }


    @Provides
    @ApplicationScope
    fun okHttpClient(loggingInterceptor: HttpLoggingInterceptor, cache: Cache): OkHttpClient {
        return OkHttpClient().newBuilder().cache(cache)
                .addInterceptor(loggingInterceptor).build()
    }

    @Provides
    @ApplicationScope
    fun stackexchangeNetworkConfiguration(): NetworkConfiguration {
        return NetworkConfiguration("https://api.stackexchange.com/2.2/")
    }
}
