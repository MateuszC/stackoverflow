package pl.lightsoft.stackoverflow.application.configuration

import android.app.Application
import timber.log.Timber

/**
 * Created by Mateusz on 2017-11-28.
 */
class TimberConfiguration : AppConfiguration
{
    override fun configureApplication(application: Application) {
        Timber.plant(Timber.DebugTree())
    }

}