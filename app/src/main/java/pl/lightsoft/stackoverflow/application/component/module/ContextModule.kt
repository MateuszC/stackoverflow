package pl.lightsoft.stackoverflow.application.component.module

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides

/**
 * Created by Mateusz on 2017-11-29.
 */
@Module
class ContextModule {
    @Provides
    @ApplicationScope
    fun context(application: Application): Context {
        return application.applicationContext
    }

}