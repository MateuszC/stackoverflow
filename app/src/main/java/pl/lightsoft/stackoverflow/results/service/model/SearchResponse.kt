package pl.lightsoft.stackoverflow.results.service.model

import com.google.gson.annotations.SerializedName

/**
 * Created by Mateusz on 2017-11-29.
 */
class SearchResponse(@SerializedName("total") val total: Int, @SerializedName("items") val items: List<Question>)