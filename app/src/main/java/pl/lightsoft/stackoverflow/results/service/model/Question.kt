package pl.lightsoft.stackoverflow.results.service.model

import com.google.gson.annotations.SerializedName

/**
 * Created by Mateusz on 2017-11-29.
 */
class Question(@SerializedName("tags") val tags: List<String>,
               @SerializedName("score") val score: Int,
               @SerializedName("question_id") val questionId: Int,
               @SerializedName("title") val title: String,
               @SerializedName("owner") val owner: Owner)