package pl.lightsoft.stackoverflow.results.model

/**
 * Created by Mateusz on 2017-11-27.
 */
class SearchResult(val title: CharSequence,
                   val author: CharSequence,
                   val tags: List<CharSequence>,
                   val points: Int,
                   val id: Int)