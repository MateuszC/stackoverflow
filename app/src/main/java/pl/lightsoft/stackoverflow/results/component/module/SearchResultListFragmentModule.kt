package pl.lightsoft.stackoverflow.results.component.module

import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import dagger.Module
import dagger.Provides
import pl.lightsoft.stackoverflow.R
import pl.lightsoft.stackoverflow.databinding.FragmentSearchResultsBinding
import pl.lightsoft.stackoverflow.message.connection.RetryConnectionMessageCreator
import pl.lightsoft.stackoverflow.message.connection.RetryConnectionSnackbar
import pl.lightsoft.stackoverflow.results.SearchResultListFragment
import pl.lightsoft.stackoverflow.results.SearchResultListViewModel
import pl.lightsoft.stackoverflow.results.list.QuestionsSearcher
import pl.lightsoft.stackoverflow.results.list.SearchResultDataSource
import pl.lightsoft.stackoverflow.results.list.SearchResultListAdapter
import pl.lightsoft.stackoverflow.results.service.QuestionSearchWebService
import pl.lightsoft.stackoverflow.results.service.ServiceQuestionSearcher
import retrofit2.Retrofit

/**
 * Created by Mateusz on 2017-11-29.
 */
@Module(includes = arrayOf(PagedListModule::class))
class SearchResultListFragmentModule {

    @Provides
    @ResultFragmentScope
    fun questionSearchWebService(retrofit: Retrofit): QuestionSearchWebService {
        return retrofit.create(QuestionSearchWebService::class.java)
    }

    @Provides
    @ResultFragmentScope
    fun searchResultListViewModel(fragment: SearchResultListFragment): SearchResultListViewModel {
        return ViewModelProviders.of(fragment).get(SearchResultListViewModel::class.java)
    }

    @Provides
    @ResultFragmentScope
    fun questionSearcher(questionSearchWebService: QuestionSearchWebService): QuestionsSearcher {
        return ServiceQuestionSearcher(questionSearchWebService)
    }



    @Provides
    @ResultFragmentScope
    fun retryConnectionMessage(fragment: SearchResultListFragment,
                               binding: FragmentSearchResultsBinding): RetryConnectionMessageCreator {
        val retryConnection = RetryConnectionSnackbar(binding.root,
                R.string.connection_error_message)
        fragment.lifecycle.addObserver(retryConnection)
        return retryConnection
    }

    @Provides
    @ResultFragmentScope
    fun query(fragment: SearchResultListFragment): String {
        return fragment.arguments?.getString(SearchResultListFragment.QUERY_KEY, "")!!
    }

    @Provides
    @ResultFragmentScope
    fun fragmentSearchResultsBinding(context: Context,
                                     model: SearchResultListViewModel): FragmentSearchResultsBinding {
        val binding = FragmentSearchResultsBinding.inflate(LayoutInflater.from(context))
        binding.resultsList.adapter = SearchResultListAdapter()
        binding.resultsList.layoutManager = LinearLayoutManager(context)
        binding.resultsList.addItemDecoration(DividerItemDecoration(context,
                DividerItemDecoration.VERTICAL))
        binding.viewModel = model
        binding.executePendingBindings()
        return binding
    }
}