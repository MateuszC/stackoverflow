package pl.lightsoft.stackoverflow.results.executor

import io.reactivex.Observable
import java.util.concurrent.Executor

/**
 * Created by Mateusz on 2017-12-06.
 */
interface TaskExecutor : Executor {
    val errors: Observable<Throwable>
}