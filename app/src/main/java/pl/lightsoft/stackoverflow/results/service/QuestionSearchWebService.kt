package pl.lightsoft.stackoverflow.results.service

import pl.lightsoft.stackoverflow.results.service.model.SearchResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by Mateusz on 2017-11-28.
 */
interface QuestionSearchWebService
{
    @GET("search?order=desc&sort=votes&site=stackoverflow&filter=!4)7xaKyZGD5lI-1H6")
    fun searchQuestions(@Query("page") page : Int, @Query("pagesize") pageSize : Int, @Query("intitle") query : String) : Call<SearchResponse>
}