package pl.lightsoft.stackoverflow.results

import android.arch.lifecycle.ViewModel
import android.arch.paging.PagedList
import android.databinding.ObservableField
import android.view.View
import pl.lightsoft.stackoverflow.results.executor.state.IdleState
import pl.lightsoft.stackoverflow.results.executor.state.LoadingState
import pl.lightsoft.stackoverflow.results.executor.state.NetworkState
import pl.lightsoft.stackoverflow.results.model.SearchResult

/**
 * Created by Mateusz on 2017-11-27.
 */
class SearchResultListViewModel : ViewModel() {
    var searchResultsList = ObservableField<PagedList<SearchResult>>()
    var progressVisibility = ObservableField<Int>()
    var networkError = ObservableField<Throwable>()

    fun onNetworkError(exception: Throwable) {
        networkError.set(exception)
    }

    fun onNetworkStateChanged(networkState: NetworkState) {
        if (networkState is IdleState) {
            progressVisibility.set(View.GONE)
        } else if (networkState is LoadingState) {
            progressVisibility.set(View.VISIBLE)
        }
    }

}