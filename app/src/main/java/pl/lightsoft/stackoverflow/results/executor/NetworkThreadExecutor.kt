package pl.lightsoft.stackoverflow.results.executor

import io.reactivex.Completable
import io.reactivex.Scheduler
import io.reactivex.subjects.PublishSubject
import pl.lightsoft.stackoverflow.results.executor.state.IdleState
import pl.lightsoft.stackoverflow.results.executor.state.LoadingState
import pl.lightsoft.stackoverflow.results.executor.state.NetworkState

/**
 * Created by Mateusz on 2017-12-06.
 */
class NetworkThreadExecutor(private val networkScheduler: Scheduler) :
        TaskExecutor {
    override val errors = PublishSubject.create<Throwable>()
    val networkState = PublishSubject.create<NetworkState>()

    private var networkCallCounter = 0
        set(value) {
            refreshNetworkState(field, value)
            field = value
        }

    private fun refreshNetworkState(oldCounter: Int, newCounter: Int) {
        if (newCounter == 0 && oldCounter != newCounter) networkState.onNext(IdleState())
        else if (newCounter != oldCounter) networkState.onNext(LoadingState())
    }

    override fun execute(p: Runnable?) {
        Completable.fromRunnable(p).subscribeOn(networkScheduler).subscribe({ networkCallCounter-- },
                this::onError)
        networkCallCounter++
    }

    private fun onError(throwable: Throwable) {
        networkCallCounter--
        errors.onNext(throwable)
    }


}