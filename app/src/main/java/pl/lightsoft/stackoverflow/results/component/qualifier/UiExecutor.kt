package pl.lightsoft.stackoverflow.results.component.qualifier

import javax.inject.Qualifier

/**
 * Created by Mateusz on 2017-12-06.
 */
@Qualifier
annotation class UiExecutor