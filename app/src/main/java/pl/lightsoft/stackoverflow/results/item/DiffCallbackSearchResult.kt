package pl.lightsoft.stackoverflow.results.item

import android.support.v7.recyclerview.extensions.DiffCallback
import pl.lightsoft.stackoverflow.results.model.SearchResult

/**
 * Created by Mateusz on 2017-11-27.
 */
class DiffCallbackSearchResult : DiffCallback<SearchResult>() {
    override fun areItemsTheSame(oldItem: SearchResult, newItem: SearchResult): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: SearchResult, newItem: SearchResult): Boolean {
        return oldItem == newItem
    }

}