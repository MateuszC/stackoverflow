package pl.lightsoft.stackoverflow.results

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.arch.paging.PagedList
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dagger.android.support.AndroidSupportInjection
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import pl.lightsoft.stackoverflow.databinding.FragmentSearchResultsBinding
import pl.lightsoft.stackoverflow.message.connection.RetryConnectionMessageCreator
import pl.lightsoft.stackoverflow.results.executor.state.IdleState
import pl.lightsoft.stackoverflow.results.executor.state.LoadingState
import pl.lightsoft.stackoverflow.results.executor.state.NetworkState
import pl.lightsoft.stackoverflow.results.list.PagedListFactory
import pl.lightsoft.stackoverflow.results.model.SearchResult
import javax.inject.Inject

/**
 * Created by Mateusz on 2017-11-27.
 */
class SearchResultListFragment : Fragment() {

    companion object {
        val QUERY_KEY = "QUERY"
        fun forQuery(query: String): Fragment {
            val args = Bundle()
            args.putString(QUERY_KEY, query)

            val fragment = SearchResultListFragment()
            fragment.arguments = args
            return fragment
        }
    }

    @Inject
    lateinit var viewModel: SearchResultListViewModel

    @Inject
    lateinit var binding: FragmentSearchResultsBinding


    private val questionsList = MutableLiveData<PagedList<SearchResult>>()

    @Inject
    lateinit var retryConnectionMessage: RetryConnectionMessageCreator

    @Inject
    lateinit var pagedListFactory: PagedListFactory

    @Inject
    lateinit var networkErrors: Observable<Throwable>

    @Inject
    lateinit var networkState: Observable<NetworkState>

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        networkErrors.subscribe(this::showRetryMessaged)
        networkState.subscribe(viewModel::onNetworkStateChanged)
        setupPagedList()
        questionsList.observe(this, Observer(viewModel.searchResultsList::set))
    }



    private fun showRetryMessaged(exception: Throwable) {
        retryConnectionMessage.showRetryMessage {
            setupPagedList()
        }
    }

    private fun setupPagedList() {
        pagedListFactory.createPagedList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ questionsList.value = it }, this::showRetryMessaged)
    }


    override fun onCreateView(inflater: LayoutInflater?,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return binding.root
    }
}