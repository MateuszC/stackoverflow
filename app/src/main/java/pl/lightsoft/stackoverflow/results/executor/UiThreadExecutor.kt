package pl.lightsoft.stackoverflow.results.executor

import android.os.Handler
import android.os.Looper
import io.reactivex.subjects.PublishSubject

/**
 * Created by Mateusz on 2017-12-06.
 */
class UiThreadExecutor(val mainLooper: Looper) : TaskExecutor {

    override val errors = PublishSubject.create<Throwable>()

    override fun execute(task: Runnable?) {
        try {
            Handler(mainLooper).post(task)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }
}