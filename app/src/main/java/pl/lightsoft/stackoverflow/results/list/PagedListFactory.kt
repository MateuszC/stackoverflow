package pl.lightsoft.stackoverflow.results.list

import android.arch.paging.PagedList
import io.reactivex.Observable
import pl.lightsoft.stackoverflow.results.model.SearchResult
import java.util.concurrent.Executor

/**
 * Created by Mateusz on 2017-12-06.
 */
class PagedListFactory(val searchResultDataSource: SearchResultDataSource,
                       val mainThreadExecutor: Executor,
                       val networkThreadExecutor: Executor) {
    fun createPagedList(): Observable<PagedList<SearchResult>> {
        return Observable.fromCallable({ buildPageList() })
    }

    private fun buildPageList(): PagedList<SearchResult> {
        val config = PagedList.Config.Builder().setEnablePlaceholders(true).setPageSize(30).build()
        return PagedList.Builder<Int, SearchResult>()
                .setDataSource(searchResultDataSource)
                .setMainThreadExecutor(mainThreadExecutor)
                .setBackgroundThreadExecutor(networkThreadExecutor)
                .setConfig(config)
                .setInitialKey(0)
                .build()
    }

}