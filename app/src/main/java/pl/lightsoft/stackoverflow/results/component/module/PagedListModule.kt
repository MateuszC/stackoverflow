package pl.lightsoft.stackoverflow.results.component.module

import android.content.Context
import dagger.Module
import dagger.Provides
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import pl.lightsoft.stackoverflow.results.component.qualifier.NetworkExecutor
import pl.lightsoft.stackoverflow.results.component.qualifier.UiExecutor
import pl.lightsoft.stackoverflow.results.executor.NetworkThreadExecutor
import pl.lightsoft.stackoverflow.results.executor.TaskExecutor
import pl.lightsoft.stackoverflow.results.executor.UiThreadExecutor
import pl.lightsoft.stackoverflow.results.executor.state.NetworkState
import pl.lightsoft.stackoverflow.results.list.PagedListFactory
import pl.lightsoft.stackoverflow.results.list.QuestionsSearcher
import pl.lightsoft.stackoverflow.results.list.SearchResultDataSource

/**
 * Created by Mateusz on 2017-12-06.
 */
@Module
class PagedListModule {


    @Provides
    @ResultFragmentScope
    fun resultDataSource(questionSearcher: QuestionsSearcher,
                         query: String): SearchResultDataSource {
        return SearchResultDataSource(questionSearcher, query)
    }

    @Provides
    @ResultFragmentScope
    @NetworkExecutor
    fun networkExecutor(networkExecutor: NetworkThreadExecutor): TaskExecutor {
        return networkExecutor
    }


    @Provides
    @ResultFragmentScope
    @UiExecutor
    fun uiExecutor(context: Context): TaskExecutor = UiThreadExecutor(
            context.mainLooper)

    @Provides
    @ResultFragmentScope
    fun networkTaskExecutor(): NetworkThreadExecutor {
        return NetworkThreadExecutor(Schedulers.io())
    }

    @Provides
    @ResultFragmentScope
    fun networkStates(networkExecutor: NetworkThreadExecutor): Observable<NetworkState> {
        return networkExecutor.networkState
    }

    @Provides
    @ResultFragmentScope
    fun networkErrors(@NetworkExecutor networkExecutor: TaskExecutor): Observable<Throwable> {
        return networkExecutor.errors
    }

    @Provides
    @ResultFragmentScope
    fun pagedListFactory(searchResultDataSource: SearchResultDataSource,
                         @UiExecutor uiExecutor: TaskExecutor,
                         @NetworkExecutor networkExecutor: TaskExecutor): PagedListFactory {
        return PagedListFactory(searchResultDataSource,
                uiExecutor,
                networkExecutor)
    }
}
