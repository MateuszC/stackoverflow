package pl.lightsoft.stackoverflow.results.component

import dagger.Subcomponent
import dagger.android.AndroidInjector
import pl.lightsoft.stackoverflow.results.SearchResultListFragment
import pl.lightsoft.stackoverflow.results.component.module.SearchResultListFragmentModule

/**
 * Created by Mateusz on 2017-11-29.
 */
@Subcomponent(modules = arrayOf(SearchResultListFragmentModule::class))
interface SearchResultListFragmentComponent : AndroidInjector<SearchResultListFragment> {
    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<SearchResultListFragment>()
}