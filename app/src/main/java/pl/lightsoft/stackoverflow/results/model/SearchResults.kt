package pl.lightsoft.stackoverflow.results.model

import pl.lightsoft.stackoverflow.results.model.SearchResult

/**
 * Created by Mateusz on 2017-11-28.
 */
class SearchResults(val total : Int, val results : List<SearchResult>)