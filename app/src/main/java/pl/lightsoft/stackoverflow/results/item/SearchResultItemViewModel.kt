package pl.lightsoft.stackoverflow.results.item

import android.arch.lifecycle.ViewModel
import android.databinding.*

/**
 * Created by Mateusz on 2017-11-27.
 */
class SearchResultItemViewModel : ViewModel() {

    var title = ObservableField<CharSequence>()
    var author = ObservableField<CharSequence>()
    var tags = ObservableField<CharSequence>()
    var points = ObservableField<CharSequence>()
    var id = ObservableInt()

    fun onItemClicked() {

    }
}