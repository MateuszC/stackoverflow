package pl.lightsoft.stackoverflow.results.list

import pl.lightsoft.stackoverflow.results.model.SearchResults

/**
 * Created by Mateusz on 2017-11-27.
 */
interface QuestionsSearcher {
    fun searchQuestions(query: String, startPosition: Int, count: Int): SearchResults?
}