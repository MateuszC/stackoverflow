package pl.lightsoft.stackoverflow.results.list

import android.arch.paging.PagedListAdapter
import android.graphics.Color
import android.text.Spannable
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.style.BackgroundColorSpan
import android.view.LayoutInflater
import android.view.ViewGroup
import pl.lightsoft.stackoverflow.databinding.SearchResultItemBinding
import pl.lightsoft.stackoverflow.results.item.DiffCallbackSearchResult
import pl.lightsoft.stackoverflow.results.item.SearchResultItemViewHolder
import pl.lightsoft.stackoverflow.results.item.SearchResultItemViewModel
import pl.lightsoft.stackoverflow.results.model.SearchResult
import java.util.Arrays.asList

/**
 * Created by Mateusz on 2017-11-27.
 */
class SearchResultListAdapter : PagedListAdapter<SearchResult, SearchResultItemViewHolder>(
        DiffCallbackSearchResult()) {
    override fun onBindViewHolder(holder: SearchResultItemViewHolder?, position: Int) {
        if (position in 0..(itemCount - 1)) {
            val searchResult = getItem(position)
            if (searchResult != null) {
                holder?.bindSearchResult(searchResult)
            } else {
                holder?.bindEmpty()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): SearchResultItemViewHolder {
        val binding = SearchResultItemBinding.inflate(LayoutInflater.from(parent?.context), parent,
                false)
        return SearchResultItemViewHolder(binding, SearchResultItemViewModel())
    }


}