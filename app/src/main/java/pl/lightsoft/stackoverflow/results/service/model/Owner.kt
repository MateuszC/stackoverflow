package pl.lightsoft.stackoverflow.results.service.model

import com.google.gson.annotations.SerializedName

/**
 * Created by Mateusz on 2017-11-29.
 */
class Owner(@SerializedName("display_name") val name : String)