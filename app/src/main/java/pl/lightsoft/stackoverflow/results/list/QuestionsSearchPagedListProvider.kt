package pl.lightsoft.stackoverflow.results.list

import android.arch.paging.DataSource
import android.arch.paging.LivePagedListProvider
import pl.lightsoft.stackoverflow.results.list.SearchResultDataSource
import pl.lightsoft.stackoverflow.results.model.SearchResult
import pl.lightsoft.stackoverflow.results.list.QuestionsSearcher

/**
 * Created by Mateusz on 2017-11-27.
 */
class QuestionsSearchPagedListProvider(private val searchResultDataSource: SearchResultDataSource) :
        LivePagedListProvider<Int, SearchResult>() {
    public override fun createDataSource(): DataSource<Int, SearchResult> {
        return searchResultDataSource
    }

}