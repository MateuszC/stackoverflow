package pl.lightsoft.stackoverflow.results.item

import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.TextUtils
import android.text.style.BackgroundColorSpan
import pl.lightsoft.stackoverflow.databinding.SearchResultItemBinding
import pl.lightsoft.stackoverflow.results.model.SearchResult

/**
 * Created by Mateusz on 2017-11-27.
 */
class SearchResultItemViewHolder(searchResultItemBinding: SearchResultItemBinding,
                                 private val searchResultItemViewModel: SearchResultItemViewModel) : RecyclerView.ViewHolder(searchResultItemBinding.root) {

    init {
        searchResultItemBinding.viewModel = searchResultItemViewModel
    }

    fun bindSearchResult(searchResult: SearchResult) {
        searchResultItemViewModel.title.set(searchResult.title)
        searchResultItemViewModel.author.set(searchResult.author)
        searchResultItemViewModel.id.set(searchResult.id)
        searchResultItemViewModel.points.set(searchResult.points.toString())
        var joinedTags = TextUtils.join(", ", searchResult.tags)
        searchResultItemViewModel.tags.set(joinedTags)
    }

    fun bindEmpty() {
        val str = SpannableStringBuilder(String.format("%100s", ""))
        str.setSpan(BackgroundColorSpan(Color.LTGRAY), 0, str.length-1, Spannable.SPAN_INCLUSIVE_INCLUSIVE)

        searchResultItemViewModel.title.set(str)
        searchResultItemViewModel.author.set(str)
        searchResultItemViewModel.tags.set(str)
        searchResultItemViewModel.id.set(-1)
        searchResultItemViewModel.points.set("")
    }

}