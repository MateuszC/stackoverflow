package pl.lightsoft.stackoverflow.results.service

import pl.lightsoft.stackoverflow.exception.NetworkNotAvailableException
import pl.lightsoft.stackoverflow.exception.ServerNotAvailableException
import pl.lightsoft.stackoverflow.results.list.QuestionsSearcher
import pl.lightsoft.stackoverflow.results.model.SearchResult
import pl.lightsoft.stackoverflow.results.model.SearchResults
import pl.lightsoft.stackoverflow.results.service.model.SearchResponse
import retrofit2.Call
import retrofit2.Response
import java.io.IOException

/**
 * Created by Mateusz on 2017-11-28.
 */
class ServiceQuestionSearcher(private val questionSearchWebService: QuestionSearchWebService) :
        QuestionsSearcher {
    override fun searchQuestions(query: String, startPosition: Int, count: Int): SearchResults? {
        val page = calculatePage(startPosition, count)
        val searchCall = questionSearchWebService.searchQuestions(page, count, query)
        return executeSearch(searchCall)
    }

    private fun calculatePage(startPosition: Int,
                              count: Int) = if (count != 0) startPosition / count + 1 else 1

    private fun executeSearch(searchCall: Call<SearchResponse>): SearchResults? {
        try {
            val response = searchCall.execute()
            return retrieveSearchResult(response)
        } catch (e: IOException) {
            throw NetworkNotAvailableException()
        }
    }

    private fun retrieveSearchResult(response: Response<SearchResponse>): SearchResults? {
        if (response.isSuccessful && response.code() == 200) {
            return convertToSearchResult(response.body())
        } else {
            throw ServerNotAvailableException()
        }
    }

    private fun convertToSearchResult(response: SearchResponse?): SearchResults? {
        if (response != null) {
            return SearchResults(response.total,
                    response.items.map {
                        SearchResult(it.title,
                                it.owner.name,
                                it.tags,
                                it.score,
                                it.questionId)
                    })
        }
        return null
    }

}