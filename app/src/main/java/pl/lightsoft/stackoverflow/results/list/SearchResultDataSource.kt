package pl.lightsoft.stackoverflow.results.list

import android.arch.paging.DataSource
import android.arch.paging.TiledDataSource
import io.reactivex.subjects.PublishSubject
import pl.lightsoft.stackoverflow.results.model.SearchResult
import pl.lightsoft.stackoverflow.results.model.SearchResults

/**
 * Created by Mateusz on 2017-11-27.
 */
class SearchResultDataSource(private val questionsSearcher: QuestionsSearcher,
                             private val query: String) : TiledDataSource<SearchResult>() {

    private var countItems = DataSource.COUNT_UNDEFINED

    override fun countItems(): Int {
        if (countItems == DataSource.COUNT_UNDEFINED) {
            searchQuestions(1, 0)?.let { countItems = it.total }
        }
        return countItems
    }

    override fun loadRange(startPosition: Int, count: Int): MutableList<SearchResult>? {
        return questionsSearcher.searchQuestions(query, startPosition, count)?.results?.toMutableList()
    }

    private fun searchQuestions(startPosition: Int, count: Int): SearchResults? {
        return questionsSearcher.searchQuestions(query, startPosition, count)
    }

}