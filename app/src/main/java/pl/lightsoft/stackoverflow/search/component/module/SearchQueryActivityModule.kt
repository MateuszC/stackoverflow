package pl.lightsoft.stackoverflow.search.component.module

import android.app.SearchManager
import android.app.SearchableInfo
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.support.v4.app.FragmentManager
import android.view.LayoutInflater
import dagger.Module
import dagger.Provides
import pl.lightsoft.stackoverflow.R
import pl.lightsoft.stackoverflow.databinding.ActivitySearchQuestionsBinding
import pl.lightsoft.stackoverflow.search.*

/**
 * Created by Mateusz on 2017-11-28.
 */
@Module
class SearchQueryActivityModule {
    @SearchActivityScope
    @Provides
    fun searchActivityBinding(searchQuestionsActivity: SearchQuestionsActivity,
                              searchableInfo: SearchableInfo,
                              searchQuestionsViewModel: SearchQuestionsViewModel): ActivitySearchQuestionsBinding {
        val searchActivityBinding = ActivitySearchQuestionsBinding
                .inflate(LayoutInflater.from(searchQuestionsActivity))
        searchActivityBinding.searchView.setSearchableInfo(searchableInfo)
        searchActivityBinding.viewModel = searchQuestionsViewModel
        return searchActivityBinding
    }

    @SearchActivityScope
    @Provides
    fun searchableInfo(searchManager: SearchManager,
                       searchQuestionsActivity: SearchQuestionsActivity): SearchableInfo {
        return searchManager.getSearchableInfo(searchQuestionsActivity.componentName)
    }

    @SearchActivityScope
    @Provides
    fun searchManager(searchQuestionsActivity: SearchQuestionsActivity): SearchManager {
        return searchQuestionsActivity.getSystemService(Context.SEARCH_SERVICE) as SearchManager
    }

    @SearchActivityScope
    @Provides
    fun searchResultsReplacer(supportFragmentManager: FragmentManager,
                              resultsFragmentFactory: ResultsFragmentFactory): SearchResultsReplacer {
        return SearchResultsReplacer(supportFragmentManager, resultsFragmentFactory, R.id.resultContainer)
    }

    @SearchActivityScope
    @Provides
    fun fragmentManager(searchQuestionsActivity: SearchQuestionsActivity): FragmentManager {
        return searchQuestionsActivity.supportFragmentManager
    }

    @SearchActivityScope
    @Provides
    fun searchQuestionsViewModel(activity: SearchQuestionsActivity): SearchQuestionsViewModel {
        return ViewModelProviders.of(activity).get(SearchQuestionsViewModel::class.java)
    }

    @SearchActivityScope
    @Provides
    fun searchQueryIntentDecoder(): IntentDecoder<String> {
        return SearchQueryIntentDecoder()
    }
}

