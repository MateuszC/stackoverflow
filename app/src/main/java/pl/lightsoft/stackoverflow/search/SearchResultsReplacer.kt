package pl.lightsoft.stackoverflow.search

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import android.support.v4.app.FragmentManager

/**
 * Created by Mateusz on 2017-11-28.
 */
class SearchResultsReplacer(private var supportFragmentManager: FragmentManager?,
                            private val resultsFragmentFactory: ResultsFragmentFactory,
                            private val fragmentContainer: Int) : LifecycleObserver {
    fun showFragmentWithResults(query: String) {
        supportFragmentManager?.let {
            val resultsFragment = resultsFragmentFactory.createResultFragment(query)
            it.beginTransaction()
                    .replace(fragmentContainer, resultsFragment)
                    .commit()
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun disableFragmentManager() {
        supportFragmentManager = null
    }
}