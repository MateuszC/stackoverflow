package pl.lightsoft.stackoverflow.search

import android.app.SearchManager
import android.content.Intent
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

/**
 * Created by Mateusz on 2017-11-27.
 */
class SearchQueryIntentDecoder @Inject constructor() : IntentDecoder<String> {
    override val searchQuery = BehaviorSubject.create<String>()!!

    override fun decodeIntent(intent: Intent) {
        if (Intent.ACTION_SEARCH == intent.action) {
            searchQuery.onNext(intent.getStringExtra(SearchManager.QUERY))
        }
    }
}
