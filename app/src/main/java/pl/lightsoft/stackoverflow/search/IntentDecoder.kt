package pl.lightsoft.stackoverflow.search

import android.content.Intent
import io.reactivex.Observable

/**
 * Created by Mateusz on 2017-11-27.
 */
interface IntentDecoder<T> {

    val searchQuery: Observable<T>

    fun decodeIntent(intent: Intent)
}