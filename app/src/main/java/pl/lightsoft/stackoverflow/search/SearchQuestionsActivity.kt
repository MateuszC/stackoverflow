package pl.lightsoft.stackoverflow.search

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import pl.lightsoft.stackoverflow.binding.RxObservableField
import pl.lightsoft.stackoverflow.databinding.ActivitySearchQuestionsBinding
import javax.inject.Inject

class SearchQuestionsActivity : AppCompatActivity(), HasSupportFragmentInjector {

    @Inject
    lateinit internal var searchActivityBinding: ActivitySearchQuestionsBinding

    @Inject
    lateinit internal var searchQueryIntentDecoder: IntentDecoder<String>

    @Inject
    lateinit internal var searchResultReplacer: SearchResultsReplacer

    @Inject
    lateinit internal var searchQuestionsViewModel: SearchQuestionsViewModel

    @Inject
    internal lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setupContentView()
        setupToolbar()
        registerLifecycleObservers()
        searchQueryIntentDecoder.searchQuery.subscribe(searchQuestionsViewModel::onNewQuerySubmit)
        RxObservableField(searchQuestionsViewModel.searchQuery).subscribe(searchResultReplacer::showFragmentWithResults)
    }

    private fun registerLifecycleObservers() {
        lifecycle.addObserver(searchResultReplacer)
    }

    private fun setupContentView() {
        setContentView(searchActivityBinding.root)
    }

    private fun setupToolbar() {
        setSupportActionBar(searchActivityBinding.searchToolbar)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        searchQueryIntentDecoder.decodeIntent(intent!!)
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
       return fragmentDispatchingAndroidInjector
    }
}
