package pl.lightsoft.stackoverflow.search

import android.support.v4.app.Fragment
import pl.lightsoft.stackoverflow.R
import pl.lightsoft.stackoverflow.message.MessageFragment
import pl.lightsoft.stackoverflow.results.SearchResultListFragment
import javax.inject.Inject

/**
 * Created by Mateusz on 2017-11-28.
 */
class ResultsFragmentFactory @Inject constructor() {
    fun createResultFragment(query: String): Fragment {
        val trimmedQuery = query.trim()
        if(trimmedQuery.isEmpty())
        {
            return MessageFragment.forMessage(R.string.empty_search_message, R.drawable.ic_cactus)
        }
        return SearchResultListFragment.forQuery(trimmedQuery)
    }
}