package pl.lightsoft.stackoverflow.search.component.module

import javax.inject.Scope

/**
 * Created by Mateusz on 2017-11-28.
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class SearchActivityScope