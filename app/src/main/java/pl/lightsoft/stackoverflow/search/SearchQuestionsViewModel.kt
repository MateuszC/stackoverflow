package pl.lightsoft.stackoverflow.search

import android.arch.lifecycle.ViewModel
import android.databinding.ObservableField

/**
 * Created by Mateusz on 2017-11-28.
 */
class SearchQuestionsViewModel : ViewModel() {
    val searchQuery = ObservableField<String>()

    fun onNewQuerySubmit(query: String) {
        searchQuery.set(query)
    }
}