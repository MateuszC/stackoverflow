package pl.lightsoft.stackoverflow.search.component

import dagger.Subcomponent
import dagger.android.AndroidInjector
import pl.lightsoft.stackoverflow.search.SearchQuestionsActivity
import pl.lightsoft.stackoverflow.search.component.module.SearchActivityScope
import pl.lightsoft.stackoverflow.search.component.module.SearchQueryActivityModule

/**
 * Created by Mateusz on 2017-11-28.
 */
@Subcomponent(modules = arrayOf(SearchQueryActivityModule::class))
interface SearchQuestionsComponent : AndroidInjector<SearchQuestionsActivity> {
    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<SearchQuestionsActivity>()
}