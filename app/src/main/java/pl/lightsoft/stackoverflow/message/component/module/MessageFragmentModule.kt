package pl.lightsoft.stackoverflow.message.component.module

import android.content.Context
import android.os.Parcelable
import android.view.LayoutInflater
import dagger.Module
import dagger.Provides
import org.parceler.Parcels
import pl.lightsoft.stackoverflow.databinding.FragmentMessageBinding
import pl.lightsoft.stackoverflow.message.Message
import pl.lightsoft.stackoverflow.message.MessageFragment

/**
 * Created by Mateusz on 04.12.2017.
 */
@Module
class MessageFragmentModule {

    @MessageFragmentScope
    @Provides
    fun message(messageFragment: MessageFragment): Message {
        val parcel = messageFragment.arguments?.getParcelable<Parcelable>(
                MessageFragment.ARG_MESSAGE)
        return Parcels.unwrap(parcel)
    }


    @MessageFragmentScope
    @Provides
    fun fragmentMessageBinding(context: Context, message: Message): FragmentMessageBinding {
        var binding = FragmentMessageBinding.inflate(LayoutInflater.from(context))
        binding.message = message
        return binding
    }
}