package pl.lightsoft.stackoverflow.message.component

import dagger.Subcomponent
import dagger.android.AndroidInjector
import pl.lightsoft.stackoverflow.message.MessageFragment
import pl.lightsoft.stackoverflow.message.component.module.MessageFragmentModule

/**
 * Created by Mateusz on 04.12.2017.
 */
@Subcomponent(modules = arrayOf(MessageFragmentModule::class))
interface MessageFragmentComponent : AndroidInjector<MessageFragment> {
    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<MessageFragment>()
}