package pl.lightsoft.stackoverflow.message

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dagger.android.support.AndroidSupportInjection
import org.parceler.Parcels
import pl.lightsoft.stackoverflow.databinding.FragmentMessageBinding
import javax.inject.Inject

/**
 * Created by Mateusz on 04.12.2017.
 */
class MessageFragment : Fragment() {
    companion object {
        val ARG_MESSAGE = "message"
        fun forMessage(messageResource: Int, iconResource: Int): MessageFragment {
            val args = Bundle()
            val userMessage = Message(messageResource,
                    iconResource)
            args.putParcelable(
                    ARG_MESSAGE, Parcels.wrap(userMessage))

            val fragment = MessageFragment()
            fragment.arguments = args
            return fragment
        }
    }

    @Inject
    internal lateinit var binding: FragmentMessageBinding

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater?,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return binding.root
    }

}