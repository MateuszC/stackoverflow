package pl.lightsoft.stackoverflow.message.connection

/**
 * Created by Mateusz on 2017-12-05.
 */
interface RetryConnectionMessageCreator {
    fun showRetryMessage(retryConnection: () -> Unit)
}