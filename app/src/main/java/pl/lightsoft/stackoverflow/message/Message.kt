package pl.lightsoft.stackoverflow.message

import org.parceler.Parcel
import org.parceler.ParcelConstructor
import org.parceler.ParcelProperty

/**
 * Created by Mateusz on 04.12.2017.
 */
@Parcel(Parcel.Serialization.BEAN)
class Message
@ParcelConstructor constructor(
        @ParcelProperty("message")
        val message: Int,
        @ParcelProperty("iconResource")
        val iconResource: Int)