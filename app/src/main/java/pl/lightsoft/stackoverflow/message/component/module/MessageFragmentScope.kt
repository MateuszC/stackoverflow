package pl.lightsoft.stackoverflow.message.component.module

import javax.inject.Scope

/**
 * Created by Mateusz on 04.12.2017.
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class MessageFragmentScope