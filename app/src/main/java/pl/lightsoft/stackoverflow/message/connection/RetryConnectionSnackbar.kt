package pl.lightsoft.stackoverflow.message.connection

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import android.support.design.widget.BaseTransientBottomBar
import android.support.design.widget.Snackbar
import android.view.View
import pl.lightsoft.stackoverflow.R

/**
 * Created by Mateusz on 2017-12-05.
 */
class RetryConnectionSnackbar(private val snackbarContainer: View, private val messageResId: Int) :
        RetryConnectionMessageCreator, LifecycleObserver {

    private var snackbar: Snackbar? = null

    private val onDismiss = object : BaseTransientBottomBar.BaseCallback<Snackbar>() {
        override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
            super.onDismissed(transientBottomBar, event)
            snackbar = null
        }
    }

    override fun showRetryMessage(retryConnection: () -> Unit) {
        if (snackbar == null) {
            snackbar = createSnackbar(retryConnection)
            snackbar!!.show()
        }
    }

    private fun createSnackbar(retryConnection: () -> Unit): Snackbar? {
        val snackbar = Snackbar.make(snackbarContainer,
                messageResId,
                Snackbar.LENGTH_INDEFINITE)
        snackbar.setAction(R.string.retry, {
            this.snackbar = null
            retryConnection()
        })
        snackbar.addCallback(onDismiss)
        return snackbar
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun dismiss() {
        snackbar?.dismiss()
    }
}
