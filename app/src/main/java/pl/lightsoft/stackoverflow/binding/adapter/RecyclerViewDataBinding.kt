package pl.lightsoft.stackoverflow.binding.adapter

import android.arch.paging.PagedList
import android.arch.paging.PagedListAdapter
import android.databinding.BindingAdapter
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView

/**
 * Created by Mateusz on 2017-11-27.
 */
@BindingAdapter("pagedList")
fun <T> setData(recyclerView: RecyclerView, pagedList: PagedList<T>?) {
    if (isPagedListAdapter(recyclerView) && pagedList != null) {
        val adapter = recyclerView.adapter as PagedListAdapter<T, *>
        adapter.setList(pagedList)
        val lm = recyclerView.layoutManager
        if (lm is LinearLayoutManager) {
            var position = lm.findFirstVisibleItemPosition()
            pagedList.loadAround(position)
        }
    }
}

private fun isPagedListAdapter(recyclerView: RecyclerView) =
        recyclerView.adapter is PagedListAdapter<*, *>
