package pl.lightsoft.stackoverflow.binding

import android.databinding.ObservableField
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import java.util.concurrent.atomic.AtomicBoolean

/**
 * Created by Mateusz on 04.12.2017.
 */
class RxObservableField<T>(private val observableField: ObservableField<T>) : Observable<T>() {

    override fun subscribeActual(observer: Observer<in T>?) {
        observer?.let {
            val propertyChangeListener = FieldPropertyChangeListener(
                    observableField, it)
            observer.onSubscribe(propertyChangeListener)
            observableField.addOnPropertyChangedCallback(propertyChangeListener)
        }
    }

    class FieldPropertyChangeListener<T>(private val observableField: ObservableField<T>,
                                         private val observer: Observer<in T>) : Disposable, android.databinding.Observable.OnPropertyChangedCallback() {

        private var disposed = AtomicBoolean()

        override fun onPropertyChanged(sender: android.databinding.Observable?, propertyId: Int) {
            if (!isDisposed && sender === observableField) {
                observer.onNext(observableField.get())
            }
        }

        override fun dispose() {
            if (disposed.compareAndSet(false, true)) {
                observableField.removeOnPropertyChangedCallback(this)
            }
        }

        override fun isDisposed(): Boolean {
            return disposed.get()
        }

    }

}
