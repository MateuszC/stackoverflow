package pl.lightsoft.stackoverflow.binding.adapter

import android.databinding.BindingAdapter
import android.support.v7.widget.SearchView

/**
 * Created by Mateusz on 2017-11-28.
 */
@BindingAdapter("initQuery")
fun setQueryWithoutSubmit(searchView: SearchView, query: String?) {
    searchView.setQuery(query, false)
}